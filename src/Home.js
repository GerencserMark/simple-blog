import { useState, useEffect } from "react";
import BlogList from "./BlogList";
import useFetch from "./useFetch";

const Home = () => {

    const { data: blogs, isPending, error} = useFetch('http://localhost:8000/blogs');

    const [name, setName] = useState("anonymus");
    const [age, setAge] = useState(24);
    const handleClick = () => {
        setName("Adam");
    }

    return ( 
        <div className="home">
            { error && <div>Could not fetch the data</div> }
            { isPending && <div>Loading...</div> }
            {blogs && <BlogList blogs={blogs} title= "All blogs" />}
            <button onClick={() => setName('bela')}>change name</button>
            <p>{ name }</p>
            
            <h2>Homepage</h2>
            <p>{ name } is { age } years old</p>
            <button onClick={handleClick}>Click me</button>
            
        </div>
     );
}
 
export default Home;